node
{
      mvnHome = tool 'Maven-3.5.0' 
      javaHome = tool 'jdk1.8.0_144'
      SonarQube = tool 'SonarQube'
     stage('Read Property File')
     {
	    try
		{
             properties = new Properties()
             File propertiesFile = new File(env.propertyfile)
             properties.load(propertiesFile.newDataInputStream())
	    }
        catch(err)
        {
		     sendFailureMail(properties.PropertyFileerror)
			 error properties.PropertyFileerror
        }		
     }
     withEnv(["PATH+JDK=$javaHome/bin"])
     {
	   env.checkout = true
	   if(env.checkout=='true')
       {
          stage('Code Checkout')
          {
		     try
			 {
                 git credentialsId: 'BitBucket', url:properties.BitBucketUrl
			 }
             catch(err)
             {
                 sendFailureMail(properties.CodeCheckOuterror)
			     error properties.CodeCheckOuterror 
             }			 
		  }
	   }
       stage('Versioning')
       {
	         try
			 {
                 File entRevisionID = new File(env.Revisionid)
	             def RevisionID=entRevisionID.text.substring(0,5)
                 version=properties.MajorVersion+'.'+properties.MinorVersion+'.'+RevisionID+'.'+env.BUILD_NUMBER
	             bat(/"${mvnHome}\bin\mvn" versions:set -DnewVersion=${version}/)
			 }
			 catch(err)
			 {
                 sendFailureMail(properties.VersioningError)
			     error properties.VersioningError 
             }	
	   }
       if(env.build=='true') 
       {
             stage('Clean,Compile,Build')
             {
			    try
				{
                     bat(/"${mvnHome}\bin\mvn" clean install/)
			    }
                catch(err)
			    {
                 sendFailureMail(properties.BuildError)
			     error properties.BuildError 
                }					
         
             }
       }
       if(env.sonar=='true'&&env.build=='true')
       {
             stage('SonarQube Analysis')
             {
                 withSonarQubeEnv('SonarQube') 
				 {
                        try
                        {						
                             bat(/"${mvnHome}\bin\mvn" clean org.jacoco:jacoco-maven-plugin:prepare-agent install/)
                             bat(/"${mvnHome}\bin\mvn" package sonar:sonar/)
						}
                        catch(err)
			            {
                             sendFailureMail(properties.SonarError)
			                 error properties.SonarError 
                        }							
                 }    
             }
    
            sleep(Integer.parseInt(properties.sleep))
            stage("Quality Gate Check") 
			{
			     try
				 {
                     timeout(time:Integer.parseInt(properties.WaitTime), unit: 'MINUTES') 
				     { 
                             def qg = waitForQualityGate() 
                             if (qg.status != 'OK') 
						    {
                             error "Pipeline aborted due to quality gate failure: ${qg.status}"
                            } 
              							
                     }
				 }
				 catch(err)
			     {
                     sendFailureMail(properties.QualityGateError)
			         error properties.QualityGateError
                 }
                 				 
            } 
        }
        if(env.artifactupload=='true'&&env.build=='true')
	    {
             stage('Upload Artifacts')
             {
			     try
				 {
                     bat(/"${mvnHome}\bin\mvn" deploy/)
				 }
                 catch(err)
                 {
				     sendFailureMail(properties.UploadArtifactError)
			         error properties.UploadArtifactError
				 }	 
              				 
             }
	    }
        if(env.deploy=='true'&&env.build=='true')
	    {  
            stage('Deploy')
            {
			    try
				{
				
                     bat(/"${mvnHome}\bin\mvn" tomcat7:deploy/)
				}
                catch(err)
                {
				     sendFailureMail(properties.DelpoymentError)
			         error properties.DelpoymentError
				}					
            }
	    }
    	
  
    }
    emailext (
	     subject: "Build Number :${env.BUILD_NUMBER} SUCCESS",
         body: " PROJECT :${env.JOB_NAME} \nBUILD NUMBER :${env.BUILD_NUMBER} \nCheck console output at : ${env.BUILD_URL}/console",
         to:properties.EmailID,
         attachLog: true
    ) 
} 
def sendFailureMail(def errormsg)
{
   emailext(
         subject: "Build Number :${env.BUILD_NUMBER} FAILED",
         body: " PROJECT :${env.JOB_NAME} \nBUILD NUMBER :${env.BUILD_NUMBER} \nCheck console output at : ${env.BUILD_URL}/console \nError:${errormsg}",
         to:properties.EmailID,
         attachLog: true
    )
}     